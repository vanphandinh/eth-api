const express = require('express');
const bodyParser = require('body-parser');
const apiRoute = require('./routes/api');
const dotenv = require('dotenv');

dotenv.config();

const app = new express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api', apiRoute);

var port = process.env.PORT || 3000;
app.listen(port, () => console.log('Listening on port ' + port));

