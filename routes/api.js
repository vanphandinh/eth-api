const express = require('express');
const router = express.Router();
const EthClient = require('../shared/helpers/eth-client');
const ethClient = new EthClient();

router.get('/getaddress/:index', (req, res) => {
    var index = req.params.index;
    ethClient.getAddress(index)
        .then((result) => {
            res.json(result);
        })
        .catch((err) =>{
            res.json({ error: err.message });
        });
});

router.get('/transaction/:txHash', (req, res) => {
    var txHash = req.params.txHash;
    ethClient.getTransaction(txHash)
        .then((result) => {
            res.json(result);
        })
        .catch((err) =>{
            res.json({ error: err.message });
        });
});

router.get('/balance/:address', (req, res) => {
    var address = req.params.address;
    ethClient.getBalance(address)
        .then((result) => {
            res.json(result);
        })
        .catch((err) =>{
            res.json({ error: err.message });
        });
});

router.get('/confirmations/:txHash', (req, res) => {
    var txHash = req.params.txHash;
    ethClient.getConfirmations(txHash)
        .then((result) => {
            res.json(result);
        })
        .catch((err) => {
            res.json({ error: err.message });
        });
});

router.post('/sendeth', (req, res) => {
    var from_index = req.body.from_index;
    var to_address = req.body.to_address;
    var value = req.body.value;
    var data = req.body.data;
    ethClient.sendETH(from_index, to_address, value, data) 
        .then((result) => {
            res.json(result);
        })
        .catch((err) =>{
            res.json({ error: err.message });
        });
});

module.exports = router;
