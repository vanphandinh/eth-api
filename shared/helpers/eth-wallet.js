const bip39 = require('bip39');
const hdkey = require('ethereumjs-wallet/hdkey');
const fs = require('fs');

const DERIVE_PATH="m/44'/1'/0'/0"; // testnet 

const mnemonic = bip39.generateMnemonic();
console.log('Mnemonic: ' + mnemonic);
bip39.mnemonicToSeed(mnemonic)
    .then((seed) => {
        const root = hdkey.fromMasterSeed(seed);
        const addrNode = root.derivePath(DERIVE_PATH);
        const publicExtendedKey = addrNode.publicExtendedKey();
        const privateExtendedKey = addrNode.privateExtendedKey();
        console.log('\nPublic Extended Key: ', publicExtendedKey);
        console.log('\nPrivate Extended Key: ', privateExtendedKey);
        fs.writeFile('wallet.txt', 'Mnemonic: ' + mnemonic + 
                                   '\nPublic Extended Key: ' + publicExtendedKey + 
                                   '\nPrivate Extended Key: ' + privateExtendedKey, 
            (error) => {
                if (!error)
                    console.log('Saved!');
            });
    });
