const Web3 = require('web3');
const dotenv = require('dotenv');
const hdkey = require('ethereumjs-wallet/hdkey');

dotenv.config();
const PROVIDER = process.env.PROVIDER;
const PUBLIC_EXTENDED_KEY = process.env.PUBLIC_EXTENDED_KEY;
const PRIVATE_EXTENDED_KEY = process.env.PRIVATE_EXTENDED_KEY;
const GAS_PRICE_IN_GWEI = process.env.GAS_PRICE_IN_GWEI;

const web3 = new Web3(PROVIDER);
const publicWallet = hdkey.fromExtendedKey(PUBLIC_EXTENDED_KEY);
const privateWallet = hdkey.fromExtendedKey(PRIVATE_EXTENDED_KEY);

class EthClient {
    getAddress(index) {
        return new Promise((resolve) => {
            var address = publicWallet.deriveChild(index).getWallet().getChecksumAddressString();
            resolve(address);
        });
    }

    getTransaction(txHash) {
        return web3.eth.getTransaction(txHash);
    }

    getBalance(address) {
        return web3.eth.getBalance(address);
    }

    async getConfirmations(txHash) {
        var tx = await web3.eth.getTransaction(txHash);
        var blockNumber = await web3.eth.getBlockNumber();
        return tx.blockNumber == null ? 0 : (blockNumber - tx.blockNumber);
    }

    async sendETH(from_index, to_addr, value, data) {
        var privateKey = privateWallet.deriveChild(from_index).getWallet().getPrivateKeyString();
        var account = web3.eth.accounts.wallet.add(privateKey);

        const tx = {
           from: account.address,
           to: to_addr,
           value: value == "max" ? 0 : value,
           data: data
        }       
        var estimateGas = await web3.eth.estimateGas(tx);
        tx.gas = estimateGas;
        tx.gasPrice = parseInt(GAS_PRICE_IN_GWEI) * 10**9; 

        if (value == "max") {
            var balance = await this.getBalance(account.address);
            var balanceBN = new web3.utils.BN(balance);
            var feeBN = (new web3.utils.BN(estimateGas * GAS_PRICE_IN_GWEI)).mul(new web3.utils.BN(1e9));
            tx.value = balanceBN.sub(feeBN).abs().toString();
        }

        return new Promise((resolve, reject) => {
            web3.eth.sendTransaction(tx, (error, txHash) => {
                web3.eth.accounts.wallet.remove(account);
                if (error) reject(error);
                else resolve(txHash);
            });
        });
    }
}

module.exports = EthClient;
